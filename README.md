# Demo Project 

[![Build Status](https://drone.3pi.dev/api/badges/3pidev/cicd-test/status.svg)](https://drone.3pi.dev/3pidev/dronetest)

An example project for CiCd(drone) on Kubernetes (Arm64). We include a default 
pipeline which can be used to deploy an real application (Front-End & Back-End) 
using different stages(DEV, TST, PROD) and gate approval. 
*This is not a complete reference but works with the way we have configured our 3pi cluster*


By default when you checking code to gitlab a webhook is triggered with the push event. This will trigger the default flow (development)
and deploys everything in the dev namespace of the project <projectname>-dev.

We have splitted the script into 4 parts:
* Development: Will test, build, publish and deploy to development
* Staging: Will deploy to the staging namespace
* Production: Will deploy to the poduction namespace
* Secrets: Will get the system wide secrets

To move a development to staging or production use the following command on node 1
```bash
drone build promote <3pidev/dronetest=projectname> <45=buildnr> <staging=target>
```

## Test

## Code Coverage

## Code Scan

## Creating a docker file

## Deploying
